# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

### Fixed

* LTR on the wrong strand: LANO_0C02124T
* LTR on the wrong strand: LANO_0H13938T
* LTR on the wrong strand: LANO_0D11298T
* Wrong Met, CDS is shorter: LANO_0F05446G
* LTR on the wrong strand: LANO_0G08988T
* mRNA with missing intron/exon: LANO_0F07514G

## v1.0 (2021-05-17)

### Added

* The 8 annotated chromosomes of Lachancea nothofagii CBS 11611 (source EBI, [GCA_900074755.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074755.1)).
