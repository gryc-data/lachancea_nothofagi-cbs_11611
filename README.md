## *Candida* (*Nakaseomyces*) *glabrata* CBS 138

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB12933](https://www.ebi.ac.uk/ena/browser/view/PRJEB12933)
* **Assembly accession**: [GCA_900074755.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074755.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: LANO0
* **Assembly length**: 11,313,793
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 1,763,880 (3)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5254
* **Pseudogene count**: 100
* **tRNA count**: 204
* **rRNA count**: 9
* **Mobile element count**: 15
